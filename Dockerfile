﻿FROM harbor.gretzki.ddns.net/containerruntimeglobal/containerruntimeglobal_x64_dotnet_runtime:latest
RUN apt-get install -y libx11-6 libx11-xcb1 libatk1.0-0 libgtk-3-0 libcups2 libdrm2 libxkbcommon0 libxcomposite1 libxdamage1 libxrandr2 libgbm1 libpango-1.0-0 libcairo2 liboss4-salsa-asound2 libxshmfence1 libnss3
RUN adduser --system runner
WORKDIR /app
COPY build_output/ ./
EXPOSE 5777
ENV ASPNETCORE_URLS=http://+:5777
USER runner
ENTRYPOINT ["dotnet", "Runner.dll"]

