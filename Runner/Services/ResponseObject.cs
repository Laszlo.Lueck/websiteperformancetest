﻿using System.Text.Json.Serialization;

namespace Runner.Services;

public class ResponseObject
{
    [JsonPropertyName("url"), JsonInclude] public string? Url;

    [JsonPropertyName("id"), JsonInclude] public string? Id;

    [JsonPropertyName("statusInt"), JsonInclude]
    public int StatusInt;

    [JsonPropertyName("timeStamp"), JsonInclude]
    public long TimeStamp;

    [JsonPropertyName("statusText"), JsonInclude]
    public string? StatusText;

    [JsonPropertyName("Headers"), JsonInclude]
    public Dictionary<string, string>? Headers;
}