﻿using Runner.Services.Inbound.JobRequest;

namespace Runner.Services;

public interface IJobMessageProcessor
{
    public Task ProcessAsync(IAsyncEnumerable<JobMessage> jobMessages);
}