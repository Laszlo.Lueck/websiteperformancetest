using LanguageExt;
using Runner.Services.Inbound.JobRequest;
using Runner.Services.Outbound.JobResponse;

namespace Runner.Services;

public interface IJobService
{
    public Task<Option<JobResponseDto>> DoJobAsync(JobRequestDto jobRequestDto, CancellationToken cancellationToken);
}