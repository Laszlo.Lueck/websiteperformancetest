using System.Diagnostics.Contracts;
using Akka;
using Akka.Streams.Dsl;
using LanguageExt;
using LanguageExt.UnsafeValueAccess;
using Runner.Services.Inbound.JobRequest;
using static LanguageExt.Prelude;

namespace Runner.Services;

public static class Utils
{
    [Pure]
    public static Option<T> ToOpt<T>(this T? value) => Optional(value);

    [Pure]
    public static Source<T, NotUsed> FilterOptionFromSource<T>(this Source<Option<T>, NotUsed> source) => source
        .Where(d => d.IsSome)
        .Select(d => d.ValueUnsafe());

    [Pure]
    public static Source<T, NotUsed> FilterLocationFromSource<T>(this Source<T, NotUsed> source, int thisLocation)
        where T : JobMessage => source.Where(d => d.Location == thisLocation);

    [Pure]
    public static async Task<Option<T>> ToOptAsync<T>(this Task<T> action)
    {
        return (await action).ToOpt();
    }

}