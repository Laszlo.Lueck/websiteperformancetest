﻿using System.Text.Json.Serialization;

namespace Runner.Services;

public class RequestObject
{
    [JsonPropertyName("url"), JsonInclude] public string? Url;

    [JsonPropertyName("id"), JsonInclude] public string? Id;

    [JsonPropertyName("timeStamp"), JsonInclude]
    public long TimeStamp;

    [JsonPropertyName("method"), JsonInclude]
    public string? Method;

    [JsonPropertyName("Headers"), JsonInclude]
    public Dictionary<string, string>? Headers;
}

public class StorageJson
{
    [JsonPropertyName("requestObject"), JsonInclude]
    public IEnumerable<RequestObject>? RequestObject;

    [JsonPropertyName("responseObject"), JsonInclude]
    public IEnumerable<ResponseObject>? ResponseObject;

}