using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.Json;
using LanguageExt;
using LanguageExt.UnsafeValueAccess;
using PuppeteerSharp;
using PuppeteerSharp.BrowserData;
using Runner.Services.Inbound.JobRequest;
using Runner.Services.Outbound.JobResponse;
using Runner.Services.Outbound.JobStore;

namespace Runner.Services;

public class JobService : IJobService
{
    private readonly ILogger _logger;
    private readonly IJobStoreRequestProducer _jobStoreRequestProducer;

    private void Bash(string cmd)
    {
        var escapedArgs = cmd.Replace("\"", "\\\"");

        var process = new Process()
        {
            StartInfo = new ProcessStartInfo
            {
                FileName = "/bin/bash",
                Arguments = $"-c \"{escapedArgs}\"",
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = true,
            }
        };
        process.Start();
        process.StandardOutput.ReadToEnd();
        process.WaitForExit();
    }

    public JobService(ILogger<JobService> logger, IJobStoreRequestProducer jobStoreRequestProducer)
    {
        _logger = logger;
        _jobStoreRequestProducer = jobStoreRequestProducer;
    }


    public async Task<Option<JobResponseDto>> DoJobAsync(JobRequestDto jobRequestDto,
        CancellationToken cancellationToken)
    {
        var browserPath = Path.GetFullPath("./browser");

        if (!Directory.Exists(browserPath))
            Directory.CreateDirectory(browserPath);

        var bfOptions = new BrowserFetcherOptions { Path = browserPath, Browser = SupportedBrowser.Chrome };

        var bf = new BrowserFetcher(bfOptions);
        var installedBrowserList = bf.GetInstalledBrowsers();
        if (installedBrowserList.Length() == 0)
        {
            _logger.LogInformation("no browser installed in path {BrowserPath}", browserPath);
            await bf.DownloadAsync();
            Option<InstalledBrowser> installedBrowserOpt = bf.GetInstalledBrowsers().First();
            installedBrowserOpt.Match(
                Some: installedBrowser =>
                {
                    if (!RuntimeInformation.IsOSPlatform(OSPlatform.Linux)) return;
                    _logger.LogInformation("set permission to path {Path}", installedBrowser.GetExecutablePath());
                    Bash($"chmod 777 {installedBrowser.GetExecutablePath()}");
                },
                None: () =>
                {
                    _logger.LogWarning("could not download at least one browser, gave up!");
                    throw new ArgumentException("cannot download browser");
                });
        }

        Option<InstalledBrowser> installedBrowserProcessedOpt = bf.GetInstalledBrowsers().First();
        return await installedBrowserProcessedOpt.Match(
            Some: async installedBrowser =>
            {
                var lo = new LaunchOptions
                {
                    Headless = true, Args = new[] { "--no-sandbox" },
                    ExecutablePath = installedBrowser.GetExecutablePath()
                };
                await using var browser = await Puppeteer.LaunchAsync(lo);
                var browserVersion = await browser.GetVersionAsync();
                _logger.LogInformation("get site with browser version {BrowserVersion}", browserVersion);

                await using var page = await browser.NewPageAsync();
                var requestList = new List<RequestObject>();
                var responseList = new List<ResponseObject>();

                page.Request += pageOnRequest(requestList);
                page.Response += pageOnResponse(responseList);

                if (jobRequestDto.UserAgent is not null)
                    await page.SetUserAgentAsync(jobRequestDto.UserAgent);


                var vo = new ViewPortOptions { Width = jobRequestDto.Width, Height = jobRequestDto.Height };
                await page.SetViewportAsync(vo);

                var st = new JobState();

                if (jobRequestDto.WithTracing)
                {
                    var traceFileName = Guid.NewGuid().ToString();
                    var fullTraceFileName = $"{browserPath}/trace_{traceFileName}.json";
                    await page.Tracing.StartAsync(new TracingOptions { Path = fullTraceFileName });
                    await page.GoToAsync(jobRequestDto.Url);
                    await page.Tracing.StopAsync();

                    if (File.Exists(fullTraceFileName))
                    {
                        var data = await File.ReadAllBytesAsync(fullTraceFileName, cancellationToken);
                        var dto = new PayloadDto()
                        {
                            ContentType = "application/json",
                            PayloadTypeEnum = PayloadTypeEnum.Tracing,
                            Id = jobRequestDto.Id,
                            Data = data
                        };

                        _logger.LogInformation("sending trace information to topic");
                        var result = await _jobStoreRequestProducer.PublishAsync(dto);
                        PrintWarning(_logger, result, "tracing");
                        st.Tracings = result.IsNone;
                        _logger.LogInformation("remove local trace file {TraceFile} after sending to topic",
                            fullTraceFileName);
                        File.Delete(fullTraceFileName);
                    }
                    else
                    {
                        _logger.LogWarning("trace file in path {TraceFile} does not exist, skip pushing trace",
                            fullTraceFileName);
                    }
                }
                else
                {
                    await page.GoToAsync(jobRequestDto.Url);
                }


                if (jobRequestDto.WithNetworks)
                {
                    _logger.LogInformation("sending network information to topic");
                    _logger.LogInformation("requests count is {RequestCount}", requestList.Count);
                    _logger.LogInformation("responses count is {ResponseCount}", responseList.Count);

                    var requestJson = new StorageJson
                    {
                        RequestObject = requestList,
                        ResponseObject = responseList
                    };

                    var requestDto = new PayloadDto
                    {
                        ContentType = "application/json",
                        PayloadTypeEnum = PayloadTypeEnum.Networks,
                        Data = JsonSerializer.SerializeToUtf8Bytes(requestJson),
                        Id = jobRequestDto.Id
                    };

                    var resultOpt = await _jobStoreRequestProducer.PublishAsync(requestDto);
                    PrintWarning(_logger, resultOpt, "networks");
                    st.Networks = resultOpt.IsNone;
                }
                else
                {
                    _logger.LogWarning("not store network information trigger is {Trigger}",
                        jobRequestDto.WithNetworks);
                }

                var performanceEntriesOpt = await page
                    .EvaluateExpressionAsync("JSON.stringify(window.performance.getEntries())")
                    .ToOptAsync();


                var performanceTimingOpt = await page
                    .EvaluateExpressionAsync("JSON.stringify(window.performance.timing)")
                    .ToOptAsync();

                var screenshotOpt = jobRequestDto.WithScreenshot
                    ? await page.ScreenshotDataAsync().ToOptAsync()
                    : Option<byte[]>.None;


                await performanceTimingOpt.Match(
                    Some: async data =>
                    {
                        var dto = new PayloadDto
                        {
                            ContentType = "application/json", PayloadTypeEnum = PayloadTypeEnum.Timing,
                            Id = jobRequestDto.Id,
                            Data = Encoding.UTF8.GetBytes(data.ToString() ?? "")
                        };
                        var result = await _jobStoreRequestProducer.PublishAsync(dto);
                        PrintWarning(_logger, result, "timings");

                        st.Timings = result.IsNone;
                    },
                    None: async () =>
                        await Task.Run(() => _logger.LogWarning("No timing data available"), cancellationToken));


                await performanceEntriesOpt.Match(
                    Some: async data =>
                    {
                        var dto = new PayloadDto
                        {
                            ContentType = "application/json",
                            Id = jobRequestDto.Id,
                            PayloadTypeEnum = PayloadTypeEnum.PerformanceEntry,
                            Data = Encoding.UTF8.GetBytes(data.ToString() ?? "")
                        };

                        var result = await _jobStoreRequestProducer.PublishAsync(dto);
                        PrintWarning(_logger, result, "performanceEntries");

                        st.PerformanceEntries = result.IsNone;
                    },
                    None: async () =>
                        await Task.Run(() => _logger.LogWarning("No entries data available"), cancellationToken)
                );


                await screenshotOpt.Match(
                    Some: async bytes =>
                    {
                        var dto = new PayloadDto
                        {
                            ContentType = "image/png",
                            Id = jobRequestDto.Id,
                            PayloadTypeEnum = PayloadTypeEnum.Screenshot,
                            Data = bytes
                        };
                        var result = await _jobStoreRequestProducer.PublishAsync(dto);
                        PrintWarning(_logger, result, "screenshot");

                        st.Screenshot = result.IsNone;
                    },
                    None: async () =>
                        await Task.Run(() => _logger.LogWarning("No screenshot available"), cancellationToken)
                );

                var uhu = new JobResponseDto { Id = jobRequestDto.Id, CreateTime = DateTime.Now, JobState = st };
                return Option<JobResponseDto>.Some(uhu);
            },
            None: async () =>
            {
                _logger.LogWarning("no browser installed, gave up!");
                return await Task.FromResult(Option<JobResponseDto>.None);
            });


        static void PrintWarning(ILogger logger, Option<string> optMessage, string type)
        {
            if (optMessage.IsNone) return;

            logger.LogWarning("something went wrong while store {StoreType} to topic", type);
            logger.LogWarning("{Result}", optMessage.ValueUnsafe());
        }
    }

    private static EventHandler<ResponseCreatedEventArgs> pageOnResponse(ICollection<ResponseObject> responseList)
    {
        return (_, e) =>
        {
            var ro = new ResponseObject
            {
                Id = e.Response.Request.Id,
                Headers = e.Response.Headers,
                StatusInt = (int)e.Response.Status,
                StatusText = e.Response.Status.ToString(),
                Url = e.Response.Url,
                TimeStamp = DateTime.Now.Ticks
            };
            responseList.Add(ro);
        };
    }

    private static EventHandler<RequestEventArgs> pageOnRequest(ICollection<RequestObject> requestList)
    {
        return (_, e) =>
        {
            var ro = new RequestObject
            {
                Id = e.Request.Id,
                Method = e.Request.Method.Method,
                TimeStamp = DateTime.Now.Ticks,
                Url = e.Request.Url,
                Headers = e.Request.Headers
            };
            requestList.Add(ro);
        };
    }
}