namespace Runner.Services.Outbound.JobResponse;

public class JobResponseDto
{
    public Guid Id { get; set; } 
    public DateTime CreateTime { get; set; }
    public JobState? JobState { get; set; }
};

public class JobState
{
    public bool Screenshot { get; set; }
    public bool Timings { get; set; }

    public bool PerformanceEntries { get; set; }

    public bool Tracings { get; set; }

    public bool Networks { get; set; } = false;
};