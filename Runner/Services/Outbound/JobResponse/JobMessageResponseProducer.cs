using LanguageExt;
using Silverback.Messaging.Publishing;

namespace Runner.Services.Outbound.JobResponse;

public class JobMessageResponseProducer : IJobMessageResponseProducer
{
    private readonly IPublisher _publisher;
    private readonly ILogger<JobMessageResponseProducer> _logger;

    public JobMessageResponseProducer(IPublisher publisher, ILogger<JobMessageResponseProducer> logger)
    {
        _publisher = publisher;
        _logger = logger;
    }

    public async Task<Option<JobMessageResult>> PublishAsync(JobMessageResult messageResult)
    {
        try
        {
            await _publisher.PublishAsync(messageResult);
            return messageResult;
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, "An error occured while publishing a message to kafka");
            return Option<JobMessageResult>.None;
        }
    }
}