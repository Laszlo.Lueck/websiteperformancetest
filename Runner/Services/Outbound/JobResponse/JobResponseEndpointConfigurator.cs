﻿using Silverback.Messaging.Configuration;

namespace Runner.Services.Outbound.JobResponse
{
    public class JobResponseEndpointConfigurator : IEndpointsConfigurator
    {
        private readonly IConfigurationHandler _configurationHandler;
        private readonly ILogger _logger;

        public JobResponseEndpointConfigurator(ILogger<JobResponseEndpointConfigurator> logger,
            IConfigurationHandler configurationHandlerHandler)
        {
            _configurationHandler = configurationHandlerHandler;
            _logger = logger;
        }

        public void Configure(IEndpointsConfigurationBuilder builder)
        {
            builder
                .AddKafkaEndpoints(endpoints => endpoints
                    .Configure(config =>
                    {
                        config.BootstrapServers = _configurationHandler.GetValue<string>("KAFKA_BOOTSTRAP_SERVER");
                    })
                    .AddOutbound<JobMessageResult>(endpoint =>
                        endpoint
                            .ProduceTo(
                                _configurationHandler.GetValue<string>("KAFKA_JOBRESPONSES_TOPICNAME")
                            ).SerializeAsJson(ser => ser.UseFixedType<JobMessageResult>())
                            .EnableChunking(128000)
                    ));
        }
    }
}