﻿using LanguageExt;

namespace Runner.Services.Outbound.JobResponse;

public interface IJobMessageResponseProducer
{
    Task<Option<JobMessageResult>> PublishAsync(JobMessageResult messageResult);
}