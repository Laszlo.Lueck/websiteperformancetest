using System.Text.Json;
using System.Text.Json.Serialization;
using Runner.Objects;

namespace Runner.Services.Outbound.JobResponse;

public class JobMessageResult
{
    [JsonPropertyName("id")] public Guid Id { get; set; }
    [JsonPropertyName("cTime")] public DateTime CTime { get; set; }

    [JsonPropertyName("jobState")] public JobState? JobState { get; set; }

    public static implicit operator JobMessageResult(JobResponseDto responseDto) => new() {Id = responseDto.Id, JobState = responseDto.JobState, CTime = responseDto.CreateTime};

    public override string ToString()
    {
        return JsonSerializer.Serialize(this);
    }
}