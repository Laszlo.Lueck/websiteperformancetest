﻿using Silverback.Messaging.Configuration;

namespace Runner.Services.Outbound.JobStore;

public class JobStoreEndpointConfigurator : IEndpointsConfigurator
{
    private readonly IConfigurationHandler _configurationHandler;
    private readonly ILogger<JobStoreEndpointConfigurator> _logger;

    public JobStoreEndpointConfigurator(IConfigurationHandler configurationHandler,
        ILogger<JobStoreEndpointConfigurator> logger)
    {
        _configurationHandler = configurationHandler;
        _logger = logger;
    }


    public void Configure(IEndpointsConfigurationBuilder builder)
    {
        builder
            .AddKafkaEndpoints(endpoints => endpoints
                .Configure(config =>
                {
                    config.BootstrapServers = _configurationHandler.GetValue<string>("KAFKA_BOOTSTRAP_SERVER");
                })
                .AddOutbound<PayloadDto>(endpoint =>
                    endpoint
                        .ProduceTo(
                            _configurationHandler.GetValue<string>("KAFKA_JOBSTORAGEREQUESTS_TOPICNAME")
                        )
                        .SerializeAsJson(ser => ser.UseFixedType<PayloadDto>())
                        .EnableChunking(128000)
                ));
    }
}