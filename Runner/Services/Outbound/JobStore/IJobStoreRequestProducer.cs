﻿using LanguageExt;
using LanguageExt.TypeClasses;

namespace Runner.Services.Outbound.JobStore;

public interface IJobStoreRequestProducer
{
    public Task<Option<string>> PublishAsync(PayloadDto payloadDto);
}