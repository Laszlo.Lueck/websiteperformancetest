using System.Text;
using LanguageExt;
using Silverback.Messaging.Publishing;

namespace Runner.Services.Outbound.JobStore;

public class JobStoreRequestProducer : IJobStoreRequestProducer
{
    private readonly ILogger<JobStoreRequestProducer> _logger;
    private readonly IPublisher _publisher;

    public JobStoreRequestProducer(ILogger<JobStoreRequestProducer> logger, IPublisher publisher)
    {
        _publisher = publisher;
        _logger = logger;
    }

    public async Task<Option<string>> PublishAsync(PayloadDto payloadDto)
    {
        try
        {
            _logger.LogInformation("publish message {MessageId}", payloadDto.Id.ToString());
            await _publisher.PublishAsync(payloadDto);
            return Option<string>.None;
        }
        catch (Exception e)
        {
            _logger.LogError(e, "an exception occured while trying to publish a message");
            return e.Message;
        }
    }
}