﻿using Confluent.Kafka;
using LanguageExt.Pipes;
using Silverback.Messaging.Configuration;

namespace Runner.Services.Inbound.JobRequest;

public class JobRequestsEndpointConfigurator : IEndpointsConfigurator
{
    private readonly ILogger<JobRequestsEndpointConfigurator> _logger;
    private readonly IConfigurationHandler _configurationHandler;

    public JobRequestsEndpointConfigurator(ILogger<JobRequestsEndpointConfigurator> logger,
        IConfigurationHandler configurationHandler)
    {
        _logger = logger;
        _configurationHandler = configurationHandler;
    }

    public void Configure(IEndpointsConfigurationBuilder builder)
    {
        builder
            .AddKafkaEndpoints(endpoints => endpoints
                .Configure(config =>
                {
                    config.BootstrapServers =
                        _configurationHandler.GetValue<string>("KAFKA_BOOTSTRAP_SERVER");
                }).AddInbound(endpoint =>
                    endpoint
                        .ConsumeFrom(
                        _configurationHandler.GetValue<string>("KAFKA_JOBREQUESTS_TOPICNAME")).Configure(
                        config =>
                        {
                            config.GroupId = $"{_configurationHandler.GetValue<string>("KAFKA_JOBREQUESTS_GROUPID")}-{_configurationHandler.GetValue<string>("LOCATION")}";
                            config.AutoOffsetReset = AutoOffsetReset.Latest;
                        })
                        .ValidateMessage(throwException: true)
                        .OnError(policy =>
                        {
                            _logger.LogWarning("cannot process message");
                            policy.Skip();
                        })
                        .DeserializeJson(ser => ser.UseFixedType<JobMessage>())));
    }
}