﻿namespace Runner.Services.Inbound.JobRequest;

public class JobMessageRequestSubscriber
{
    private readonly ILogger<JobMessageRequestSubscriber> _logger;
    private readonly IJobMessageProcessor _jobMessageProcessor;

    public JobMessageRequestSubscriber(ILogger<JobMessageRequestSubscriber> logger, IJobMessageProcessor jobMessageProcessor)
    {
        _logger = logger;
        _logger.LogInformation("create new instance of {Instance}", nameof(JobMessageRequestSubscriber));
        _jobMessageProcessor = jobMessageProcessor;
    }

    public async Task OnBatchReceivedAsync(IAsyncEnumerable<JobMessage> batch)
    {
        await _jobMessageProcessor.ProcessAsync(batch);
    }
}




