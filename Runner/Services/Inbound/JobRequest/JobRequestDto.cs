using Silverback.Messaging.Messages;

namespace Runner.Services.Inbound.JobRequest;

public class JobRequestDto
{
    public Guid Id { get; set; }
    public string? Url { get; set; }

    public int Width { get; set; }

    public int Height { get; set; }

    public string? UserAgent { get; set; }

    public bool WithTimings { get; set; }

    public bool WithPerformanceEntries { get; set; }

    public bool WithScreenshot { get; set; }

    public bool WithTracing { get; set; }
    
    public bool WithNetworks { get; set; }

    public static implicit operator JobRequestDto(JobMessage jobMessage) =>
        new()
        {
            Id = jobMessage.Id, Url = jobMessage.Url, Width = jobMessage.Width, Height = jobMessage.Height,
            UserAgent = jobMessage.UserAgent, WithTracing = jobMessage.WithTracing,
            WithTimings = jobMessage.WithTimings, WithScreenshot = jobMessage.WithScreenshot,
            WithPerformanceEntries = jobMessage.WithPerformanceEntries, WithNetworks = jobMessage.WithNetworks 
        };
}