using System.Text.Json.Serialization;
using Silverback.Messaging.Messages;

namespace Runner.Services.Inbound.JobRequest;

public class JobMessage
{
    [JsonPropertyName("id")] public Guid Id { get; set; }
    [JsonPropertyName("url")] public string? Url { get; set; }

    [JsonPropertyName("userAgent")] public string? UserAgent { get; set; }
    [JsonPropertyName("width")] public int Width { get; set; }
    [JsonPropertyName("height")] public int Height { get; set; }

    [JsonPropertyName("location")] public int Location { get; set; }

    [JsonPropertyName("withTimings")] public bool WithTimings { get; set; }
    
    [JsonPropertyName("withNetworks")] public bool WithNetworks { get; set; }

    [JsonPropertyName("withPerformanceEntries")]
    public bool WithPerformanceEntries { get; set; }

    [JsonPropertyName("withScreenshot")] public bool WithScreenshot { get; set; }

    [JsonPropertyName("withTracing")] public bool WithTracing { get; set; }
}