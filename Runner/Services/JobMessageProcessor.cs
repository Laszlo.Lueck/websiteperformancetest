﻿using System.Text.Json;
using Akka.Actor;
using Akka.Streams;
using Akka.Streams.Dsl;
using Akka.Streams.Implementation.Fusing;
using LanguageExt.UnsafeValueAccess;
using Runner.Services.Inbound.JobRequest;
using Runner.Services.Outbound.JobResponse;

namespace Runner.Services;

public class JobMessageProcessor : IJobMessageProcessor
{
    private readonly ILogger<JobMessageProcessor> _logger;
    private readonly ActorSystem _actorSystem;
    private readonly IJobMessageResponseProducer _jobMessageResponseProducer;
    private readonly IJobService _jobService;
    private readonly int _iLocation;
    
    public JobMessageProcessor(ILogger<JobMessageProcessor> logger, ActorSystem actorSystem,
        IJobMessageResponseProducer jobMessageResponseProducer, IJobService jobService, IConfigurationHandler configurationHandler)
    {
        _logger = logger;
        _actorSystem = actorSystem;
        _jobMessageResponseProducer = jobMessageResponseProducer;
        _jobService = jobService;
        _iLocation = int.Parse(configurationHandler.GetValue<string>("LOCATION"));
    }

    public async Task ProcessAsync(IAsyncEnumerable<JobMessage> jobMessages)
    {
        await Source.FromGraph(new AsyncEnumerable<JobMessage>(() => jobMessages))
            .FilterLocationFromSource(_iLocation)
            .SelectAsync(1, async element => await _jobService.DoJobAsync(element, new CancellationToken()))
            .FilterOptionFromSource()
            .SelectAsync(1, async jobResponseDto => await _jobMessageResponseProducer.PublishAsync(jobResponseDto))
            .Where(d => d.IsSome)
            .Select(k => k.ValueUnsafe())
            .RunForeach(data =>
            {
                _logger.LogInformation("successfully processed job with id {JobId}", data.Id);
            }, _actorSystem.Materializer());
    }
}