using Akka.Actor;
using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Logging.Console;
using Microsoft.OpenApi.Models;
using Runner.Services;
using Runner.Services.Inbound.JobRequest;
using Runner.Services.Outbound.JobResponse;
using Runner.Services.Outbound.JobStore;

var builder = WebApplication.CreateSlimBuilder(args);
builder.Logging.AddSimpleConsole(options =>
{
    options.IncludeScopes = false;
    options.SingleLine = true;
    options.TimestampFormat = "[yyy-MM-dd HH:mm:ss.ffff] ";
    options.ColorBehavior = LoggerColorBehavior.Enabled;
});
builder.Logging.AddFilter("*", LogLevel.Information);
builder.Services.AddAuthorization();
builder.Services.AddControllers();


builder
    .Services
    .AddSilverback()
    .UseModel()
    .WithConnectionToMessageBroker(o => o.AddKafka())
    .AddEndpointsConfigurator<JobRequestsEndpointConfigurator>()
    .AddEndpointsConfigurator<JobResponseEndpointConfigurator>()
    .AddEndpointsConfigurator<JobStoreEndpointConfigurator>()
    .AddScopedSubscriber<JobMessageRequestSubscriber>();
IConfigurationHandler configHandler = new ConfigurationHandler(builder.Configuration);

builder.Services.AddSingleton(configHandler);
builder.Services.AddScoped<IJobMessageResponseProducer, JobMessageResponseProducer>();
builder.Services.AddScoped<IJobStoreRequestProducer, JobStoreRequestProducer>();
builder.Services.AddScoped<IJobMessageProcessor, JobMessageProcessor>();

builder.Services.AddSingleton(_ => ActorSystem.Create("WebSitePerformanceTest"));
builder.Configuration.AddConfiguration(new ConfigurationBuilder().AddEnvironmentVariables().Build());

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGenNewtonsoftSupport();
builder.Services.AddTransient<IJobService, JobService>();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v2", new OpenApiInfo {Title = "Runner", Version = "v2"});
    c.EnableAnnotations();
});

builder.Services
    .AddHealthChecks()
    .AddKafka(config =>
        {
            config.BootstrapServers = configHandler.GetValue<string>("KAFKA_BOOTSTRAP_SERVER");
        }, failureStatus: HealthStatus.Unhealthy, timeout: TimeSpan.FromSeconds(2), name: "KafkaHealthCheck");

var app = builder.Build();

app.Lifetime.ApplicationStarted.Register(() => { app.Services.GetService<ActorSystem>(); });
app.Lifetime.ApplicationStopping.Register(() => { app.Services.GetService<ActorSystem>()?.Terminate().Wait(); });

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger(c => { c.SerializeAsV2 = true; });
    app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v2/swagger.json", "Runner"); });
}

app.MapHealthChecks("/healthz", new HealthCheckOptions {ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse});
app.UseHttpsRedirection();

app.UseAuthorization();
app.UseStaticFiles();
app.UseRouting();
app.MapControllers();

app.Run();