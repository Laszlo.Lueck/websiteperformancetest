﻿using System.Text.Json.Serialization;

namespace Runner.Objects;


public class PerformanceEntry
{
   [JsonPropertyName("name")]
   public string? Name { get; set; }
   
   [JsonPropertyName("entryType")]
   public string? EntryType { get; set; }
   
   [JsonPropertyName("startTime")]
   public double StartTime { get; set; }
   
   [JsonPropertyName("duration")]
   public double Duration { get; set; }
   
   [JsonPropertyName("initiatorType")]
   public string? InitiatorType { get; set; }
   
   [JsonPropertyName("nextHopProtocol")]
   public string? NextHopProtocol { get; set; }
   
   [JsonPropertyName("renderBlockingStatus")]
   public string? RenderBlockingStatus { get; set; }
   
   [JsonPropertyName("workerStart")]
   public double WorkerStart { get; set; }
   
   [JsonPropertyName("redirectStart")]
   public double RedirectStart { get; set; }
   
   [JsonPropertyName("redirectEnd")]
   public double RedirectEnd { get; set; }
   
   [JsonPropertyName("fetchStart")]
   public double FetchStart { get; set; }
   
   [JsonPropertyName("domainLookupStart")]
   public double DomainLookupStart { get; set; }
   
   [JsonPropertyName("domainLookupEnd")]
   public double DomainLookupEnd { get; set; }
   
   [JsonPropertyName("connectStart")]
   public double ConnectStart { get; set; }
   
   [JsonPropertyName("connectEnd")]
   public double ConnectEnd { get; set; }
   
   [JsonPropertyName("secureConnectionStart")]
   public double SecureConnectionStart { get; set; }
   
   
   
}

/*
{
   "name":"https://www.heise.de/",
   "entryType":"navigation",
   "startTime":0,
   "duration":881.3999999910593,
   "initiatorType":"navigation",
   "nextHopProtocol":"h2",
   "renderBlockingStatus":"blocking",
   "workerStart":0,
   "redirectStart":0,
   "redirectEnd":0,
   "fetchStart":67.70000000298023,
   "domainLookupStart":68.79999999701977,
   "domainLookupEnd":72.29999999701977,
   "connectStart":72.29999999701977,
   "connectEnd":94.20000000298023,
   "secureConnectionStart":82.3999999910593,
   "requestStart":94.59999999403954,
   "responseStart":105.3999999910593,
   "responseEnd":147.8999999910593,
   "transferSize":148759,
   "encodedBodySize":148459,
   "decodedBodySize":902499,
   "responseStatus":0,
   "serverTiming":[
      
   ],
   "unloadEventStart":0,
   "unloadEventEnd":0,
   "domInteractive":399.79999999701977,
   "domContentLoadedEventStart":455.3999999910593,
   "domContentLoadedEventEnd":456.5,
   "domComplete":878.2000000029802,
   "loadEventStart":878.2999999970198,
   "loadEventEnd":881.3999999910593,
   "type":"navigate",
   "redirectCount":0,
   "activationStart":0
}
*/