using System.Text.Json.Serialization;

namespace Runner.Endpoints.SingleRequest;

public class SingleEndpointResponse
{
    [JsonPropertyName("returnId"), JsonInclude]
    public Guid ReturnId;
}