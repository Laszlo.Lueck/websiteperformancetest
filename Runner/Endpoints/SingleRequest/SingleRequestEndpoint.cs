using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Runner.Services;
using Runner.Services.Inbound.JobRequest;

namespace Runner.Endpoints.SingleRequest;

public class SingleRequestEndpoint : EndpointBaseAsync.WithRequest<SingleEndpointRequest>.WithActionResult<
    SingleEndpointResponse>
{
    private readonly ILogger<SingleRequestEndpoint> _logger;
    private readonly IJobService _jobService;

    public SingleRequestEndpoint(ILogger<SingleRequestEndpoint> logger, IJobService jobService)
    {
        _logger = logger;
        _jobService = jobService;
    }

    [HttpPost("api/singleUrl")]
    public override async Task<ActionResult<SingleEndpointResponse>> HandleAsync(SingleEndpointRequest request,
        CancellationToken cancellationToken = new CancellationToken())
    {
        var jobRequestDto = new JobRequestDto()
        {
            Height = 1920,
            Width = 1080,
            UserAgent = "Chrome",
            Id = Guid.NewGuid(),
            Url = request.Url
        };
        
        var result = await _jobService.DoJobAsync(jobRequestDto, cancellationToken);
        return result.Match(
            Some: res => Ok(res.Id),
            None: () =>
            {
                _logger.LogWarning("something went wrong");
                return (ActionResult) BadRequest();
            });
    }
}