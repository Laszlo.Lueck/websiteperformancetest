using System.Text.Json.Serialization;

namespace Runner.Endpoints.SingleRequest;

public class SingleEndpointRequest
{
    [JsonPropertyName("url"), JsonInclude] public string? Url;
}